// IDS344 - 2022-01 - Grupo 1 - QueueManagement
//
// Nikita Kravchenko - 1101607
// Omar N��ez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel B�ez - 1073597

#include <iostream>

#include "GenericFunctions.h"
#include "QueueStruct.h"

using namespace std;

int PriorityLimit = 16;

struct QueueNode
{
    string data;
    int priority;
    QueueNode* next;
};

struct Queue
{
    QueueNode* first;
    QueueNode* last;
};

struct Queue* CreateQueue()
{
    struct Queue* newQueue = new (struct Queue);

    newQueue->first = NULL;
    newQueue->last = NULL;

    return newQueue;
};

struct QueueNode* CreateQueueNode(string data, int priority) {
    struct QueueNode* newNode = new(struct QueueNode);

    newNode->data = data;
    newNode->priority = priority;
    newNode->next = NULL; //siempre el nodo recien creado de la cola apunta a NULL

    return newNode;
};

void Enqueue(Queue*& queue, string data, int priority)
{
    QueueNode* aux = CreateQueueNode(data, ModifyPriority(priority));

    if (queue->first == NULL) //si la cola est� vacia, el primer elemento es primer y �ltimo al mismo tiempo
        queue->first = aux;
    else
        queue->last->next = aux;

    queue->last = aux;        // puntero que siempre apunta al ultimo elemento 
}


string Dequeue(Queue*& queue)
{
    QueueNode* aux = queue->first;
    string data ="";

    if (aux != NULL) {
        data = aux->data;
        data += "  and priority  ";
        data += to_string(aux->priority) ;
        queue->first = aux->next;      
    }

    delete aux;
    return data;
}

bool IsQueueEmpty(Queue*& queue)
{
    QueueNode* aux = queue->first;


    if (aux == NULL) {
        delete aux;
        return true;
    }
    return false;
}

void EnqueueValue(Queue*& queue, string text, int priority)
{
    Enqueue(queue, text, priority);

    SortByPriority(queue);
}

void SortByPriority(Queue*& queue)
{
    QueueNode * aux1 = new QueueNode;
    QueueNode * aux2 = new QueueNode;

    string dataAux;
    int priorityAux;

    aux1 = queue->first;
    
    while (aux1->next != NULL)
    {
        aux2 = aux1->next;

        while (aux2 != NULL)
        {
            if (aux1->priority > aux2->priority)
            {
                dataAux = aux1->data;
                priorityAux = aux1->priority;

                aux1->data = aux2->data;
                aux1->priority = aux2->priority;

                aux2->data = dataAux;
                aux2->priority = priorityAux;
            }

            aux2 = aux2->next;
        }

        aux1 = aux1->next;
    }
}

void ShowAllQueue(Queue*& queue)
{
    QueueNode* aux = queue->first;
    string data = "";

    do
    {
        data = to_string(aux->priority);
        data += "- ";
        data += aux->data;  
        cout << data << endl;

        if (aux->next != NULL)
            aux = aux->next;
        else
            return;
    } while (true);
}

int ModifyPriority(int prior)
{
    if (prior > PriorityLimit)
        return 100;

    return prior;
}