#include <string> 

using namespace std;

void ExitProgram();
void ClearConsole(string programName);
void Pause();
bool IsNumber(string s);
