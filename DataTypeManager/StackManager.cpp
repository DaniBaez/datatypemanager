// IDS344 - 2022-01 - Grupo 1 - StackManagement
// 
// Nikita Kravchenko - 1101607
// Omar Núñez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel Báez - 1073597

#include <iostream>

#include "GenericFunctions.h" //nos permite modificar el flujo de ejecucion de las instrucciones de nuestro programa 
#include "StackManager.h" //nos permite modificar el flujo de ejecucion de las instrucciones de nuestro programa 
#include "StackStruct.h" //nos permite modificar el flujo de ejecucion de las instrucciones de nuestro programa 

using namespace std;

const string programName = "Stack Manager";

StackNode* stack = NULL; //definir el StackNode globalmente para tener acceso desde cada función
bool runStackManager;

void StartStackManager()
{
    runStackManager = true;
    
    ClearConsole(programName);

    cout << "- NOTE:\n";
    cout << "This program uses string data type to store given values.\n";
    cout << "This program follows general stacks general rules.\n\n";

    Pause();
    ClearConsole(programName);

    cout << "Welcome!\n\n";

    while (runStackManager)
    {
        DisplayStackMenu();
    }
}

// Displays program menu.
void DisplayStackMenu()
{
    string menuOptionString;
    int menuOption;

    cout << "Select an option. (1 - 4)\n";
    cout << "1. Add\n";
    cout << "2. Remove\n";
    cout << "3. Display all\n";
    cout << "4. Exit\n\n";
    cout << ">> ";

    cin >> menuOptionString;

    if (IsNumber(menuOptionString)) menuOption = stoi(menuOptionString); //stoi to check if its number
    else menuOption = 0;

    switch (menuOption)
    {
        case 1:
            AddToStack();
            break;
        case 2:
            RemoveFromStack();
            break;
        case 3:
            DisplayStack();
            break;
        case 4:
            runStackManager = false;
            break;
        default:
            cout << "Please, enter a valid option. (1 - 4)\n";
            Pause();
            break;
    }

    ClearConsole(programName);
}

// Add value to stack.
void AddToStack()
{
    string value;

    ClearConsole(programName);

    cout << "Enter the value: ";
    cin >> value;

    Push(stack, value);    
}

//Removes stack top value.
void RemoveFromStack()
{
    ClearConsole(programName);

    if (IsStackEmpty(stack))
    {
        cout << "Stack is currently empty.\n";
        Pause();
    }
    else
    {        
        cout << Pop(stack) << " removed.\n";
        Pause();
    }
}

// Shows all the stack values.
void DisplayStack()
{
    ClearConsole(programName);

    if (IsStackEmpty(stack)) cout << "Stack is currently empty.\n";
    else
    {
        cout << "- TOP -\n";

        while (!IsStackEmpty(stack)) cout << Pop(stack) << "\n";

        cout << "- BOTTOM -\n";
    }

    Pause();
}
