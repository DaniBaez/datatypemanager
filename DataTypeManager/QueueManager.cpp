// IDS344 - 2022-01 - Grupo 1 - QueueManagement
//
// Nikita Kravchenko - 1101607
// Omar N��ez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel B�ez - 1073597

#include <iostream>

#include "GenericFunctions.h"
#include "QueueManager.h"
#include "QueueStruct.h"

using namespace std;

const string programName = "Queue Manager";

Queue* myQueue = CreateQueue();


bool runQueueManager = true;

void StartQueueManager()
{
    runQueueManager = true;

    ClearConsole(programName);

    cout << "- NOTE:\n";
    cout << "This program uses string data type to store given values.\n";
    cout << "This program follows general queue general rules.\n\n";

    Pause();
    ClearConsole(programName);

    cout << "Welcome!\n\n";

    while (runQueueManager)
    {
        DisplayQueueMenu();
    }
}

void DisplayQueueMenu()
{
    string menuOptionString;
    int menuOption = 0;


    cout << "Select an option. (1 - 4)\n";
    cout << "1. Enqueue\n";
    cout << "2. Dequeue\n";
    cout << "3. Display queue\n";
    cout << "4. Exit\n\n";
    cout << ">> ";
    
    cin >> menuOptionString;

    menuOption = IsNumber(menuOptionString) ? stoi(menuOptionString) : 0;

    switch (menuOption)
    {
        case 1:
            AddToQueue();
            break;
        case 2:
            RemoveFromQueue();
            break;
        case 3:
            DisplayQueue();
            break;
        case 4:
            runQueueManager = false;

           // delete myQueue, programName, runQueueManager;
            break;
        default:
            cout << "Please, enter a valid option. (1 - 4)\n";
            Pause();
            break;
    }

    ClearConsole(programName);
}

void AddToQueue()
{
    string value;
    string priorityStr = "a";

    int priority;
    
    ClearConsole(programName);

    cout << "Enter string value: ";
    cin >> value;

    while (!IsNumber(priorityStr))
    {
        cout << "Enter priority value: ";
        cin >> priorityStr;
    }

    priority = stoi(priorityStr);

    EnqueueValue( myQueue, value, priority);
}


//Removes stack top value.
void RemoveFromQueue()
{
    ClearConsole(programName);

    if (IsQueueEmpty(myQueue)) 
        cout << "Queue is empty!";
    
    else 
        cout << Dequeue(myQueue) << " removed!";

    Pause();
}


void DisplayQueue()
{
    ClearConsole(programName);
    
    if (IsQueueEmpty(myQueue))
        cout << "Queue is empty!";
    else
        ShowAllQueue(myQueue);;

    Pause();
}
